﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using cmnmodels;

namespace dataaccesslayer
{
    public class dataclass
    {
        SqlConnection conn = new SqlConnection("Data Source=DESKTOP-D199F7V\\MSSQLSERVER01;Initial Catalog=sharath;Integrated Security=True");
        public DataSet information()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = ("select *from customerinfo");
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter dr = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            conn.Open();
            dr.Fill(ds, "test");
            conn.Close();
            return ds;
        }
           public void insertsign(dbmodels mdl)
            
           {
            try
            {
                SqlConnection conn = new SqlConnection("Data Source=DESKTOP-D199F7V\\MSSQLSERVER01;Initial Catalog=sharath;Integrated Security=True");


                SqlCommand cmn = new SqlCommand();
                 cmn.Connection = conn;
                cmn.CommandText = "customerdetails";
                cmn.CommandType = CommandType.StoredProcedure;
                cmn.Parameters.AddWithValue("@Name", mdl.Name);
                cmn.Parameters.AddWithValue("@Address", mdl.Address);
                cmn.Parameters.AddWithValue("@Contactno", mdl.Contactno);
                cmn.Parameters.AddWithValue("@Email", mdl.Email);
                conn.Open();
                cmn.ExecuteNonQuery();

            }
            catch (Exception ex)
            {

            }
            finally
            {
                conn.Close();
            }
            conn.Close();
            

           }
        public void updatemodel(dbmodels dbs)
        {
            try
            {
                SqlConnection conn = new SqlConnection("Data Source=DESKTOP-D199F7V\\MSSQLSERVER01;Initial Catalog=sharath;Integrated Security=True");


                SqlCommand cmn = new SqlCommand();
                cmn.Connection = conn;
                cmn.CommandText = "updatecustomer";
                cmn.CommandType = CommandType.StoredProcedure;
                cmn.Parameters.AddWithValue("@Id", dbs.Id);
                cmn.Parameters.AddWithValue("@Name", dbs.Name);
                cmn.Parameters.AddWithValue("@Address", dbs.Address);
                cmn.Parameters.AddWithValue("@Contctno", dbs.Contactno);
                cmn.Parameters.AddWithValue("@Email", dbs.Email);
                conn.Open();
                cmn.ExecuteNonQuery();

            }
            catch (Exception ex)
            {

            }
            finally
            {
                conn.Close();
            }
            conn.Close();


        }
        public void deletemodel(int id)
        {
            try
            {
                SqlConnection conn = new SqlConnection("Data Source=DESKTOP-D199F7V\\MSSQLSERVER01;Initial Catalog=sharath;Integrated Security=True");


                SqlCommand cmn = new SqlCommand();
                cmn.Connection = conn;
                cmn.CommandText = "Deletecustomer";
                cmn.CommandType = CommandType.StoredProcedure;
                cmn.Parameters.AddWithValue("@Id",id);
                conn.Open();
                cmn.ExecuteNonQuery();

            }
            catch (Exception ex)
            {

            }
            finally
            {
                conn.Close();
            }
            


        }




    }

}
